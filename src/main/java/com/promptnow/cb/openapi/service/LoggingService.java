package com.promptnow.cb.openapi.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface LoggingService {

    void logRequest(HttpServletRequest httpServletRequest, Object body) throws JsonProcessingException;

    void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object body) throws JsonProcessingException;
}
