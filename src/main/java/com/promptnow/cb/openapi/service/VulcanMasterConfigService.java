package com.promptnow.cb.openapi.service;

import com.promptnow.cb.openapi.entity.VulcanMasterConfigProfile;
import com.promptnow.cb.openapi.repository.VulcanMasterConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VulcanMasterConfigService {

    @Autowired
    VulcanMasterConfigRepository vulcanMasterConfigRepository;

    public Optional<VulcanMasterConfigProfile> getVulcanByName(String name){
        return vulcanMasterConfigRepository.findByName(name);
    }
}
