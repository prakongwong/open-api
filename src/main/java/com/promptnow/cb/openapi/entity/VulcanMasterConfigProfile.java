package com.promptnow.cb.openapi.entity;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity(name = "vulcan_master_config")
//@ExcludeSuperclassListeners
//public class VulcanMasterConfigProfile extends CommonEntity{
public class VulcanMasterConfigProfile {

    @Id
    @Column(name = "context_name")
    private String contextName;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    @Column(name = "active")
    private int active;

//    @Column(name = "active")
//    @Type(type = "org.hibernate.type.NumericBooleanType")
//    private boolean active;

    @Column(name = "master_flag")
    private String masterFlag;


}
