package com.promptnow.cb.openapi.repository;

import com.promptnow.cb.openapi.entity.VulcanMasterConfigProfile;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VulcanMasterConfigRepository extends CommonRepository<VulcanMasterConfigProfile, String>{

    Optional<VulcanMasterConfigProfile> findByName(String name);
}
