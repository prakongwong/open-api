package com.promptnow.cb.openapi;

import com.promptnow.cb.openapi.repository.VulcanMasterConfigRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Log4j2
@SpringBootApplication
//public class OpenapiApplication implements CommandLineRunner{
public class OpenapiApplication{

	@Autowired
	private VulcanMasterConfigRepository vulcanMasterConfigRepository;

//	private static final Logger log = LogManager.getLogger(OpenapiApplication.class);



	public static void main(String[] args) {

		System.err.close();
		SpringApplication.run(OpenapiApplication.class, args);

		log.trace("TRACE");
		log.debug("DEBUG");
		log.info("INFO");
		log.warn("WARN");
		log.error("ERROR");
		log.fatal("FATAL");
	}

//	@Override
//	public void run(String... args){
//		VulcanMasterConfigProfile p1 = new VulcanMasterConfigProfile();
//		p1.setContextName("cb-open-api");
//		p1.setName("PATH");
//		p1.setValue("C:///");
//		p1.setActive(1);
//		p1.setMasterFlag("Y");
//		vulcanMasterConfigRepository.save(p1);
//	}

//	@Override
//	public void run(String... args){
//		VulcanMasterConfigProfile p1 = new VulcanMasterConfigProfile();
//		List<VulcanMasterConfigProfile> list = vulcanMasterConfigRepository.findAll();
//
//		System.out.println(list.get(0).getValue());
//	}

}
