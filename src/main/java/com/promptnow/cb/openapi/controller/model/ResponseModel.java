package com.promptnow.cb.openapi.controller.model;

import com.promptnow.cb.openapi.dto.CommonResponseModel;
import com.promptnow.cb.openapi.exception.CommonException;

public class ResponseModel extends CommonResponseModel {

    public static final String CODE_OPERATION_SUCCESS_0000 = "0000";
    public static final String MSG_OPERATION_SUCCESS_0000 = "Operation Success.";

    public static final String CODE_NAME_NOT_MATCH_TT0101 = "TT0101";
    public static final String MSG_NAME_NOT_MATCH_TT0101 = "Name not match.";


    public void setSuccess() {
        code = CODE_OPERATION_SUCCESS_0000;
        message = MSG_OPERATION_SUCCESS_0000;
    }





}
