package com.promptnow.cb.openapi.controller.advice;

import com.promptnow.cb.openapi.exception.CommonException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@Log4j2
//@ControllerAdvice + @ResponseBody == @RestControllerAdvice
@RestControllerAdvice
public class CommonAdvice extends ResponseEntityExceptionHandler {

    @Getter
    @Setter
    private class ExceptionResponse {
        private String code;
        private String message;
        private LocalDateTime timestamp;
    }

    private ResponseEntity<Object> handle(String message, HttpStatus status, String code) {
        log.debug("handle");
        ExceptionResponse response = new ExceptionResponse();
        response.setCode(code);
        response.setMessage(message);
        response.setTimestamp(LocalDateTime.now());
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("x-info", "https://domain-name/context-path/api/v1/errors/" + code);
//        return ResponseEntity.status(status).headers(headers).body(response);
        return ResponseEntity.status(status).body(response);
    }


    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        log.debug("handleExceptionInternal");
        return handle(ex.getMessage(), status, String.valueOf(status.value()));
    }

    @ExceptionHandler(CommonException.class)
    protected ResponseEntity<Object> handleCommonException(CommonException ex) {
        return handle(ex.getMessage(), ex.getStatus(), ex.getCode());
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception ex) {
        log.debug("handleException");
        return handle(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }


}
