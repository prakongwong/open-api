package com.promptnow.cb.openapi.controller;

import com.google.gson.Gson;
import com.promptnow.cb.openapi.controller.model.ResponseModel;
import com.promptnow.cb.openapi.dto.*;
import com.promptnow.cb.openapi.entity.VulcanMasterConfigProfile;
import com.promptnow.cb.openapi.exception.BusinessException;
import com.promptnow.cb.openapi.service.VulcanMasterConfigService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Log4j2
@RestController
public class VulcanMasterConfigController {

    private final AtomicLong count = new AtomicLong();

    @Autowired
    private VulcanMasterConfigService vulcanMasterConfigService;

    @GetMapping(value = "/test")
    public void test() {
        log.debug("Testna");
    }

//    @GetMapping("/greetings/{id}")
//    public GreetingResponseDto getGreeting(@PathVariable("id") Long id) {
//        return GreetingResponseDto.builder().message1("Hello world!").build();
//    }

    @PostMapping("/greetings")
    public ResponseEntity<GreetingResponseDto> createGreeting(@RequestBody GreetingRequestDto reqObj) {
        GreetingResponseDto resObj = new GreetingResponseDto();

        resObj.message1 = "ArmYo";

//        return GreetingResponseDto.builder().id(count.incrementAndGet()).message(greetingRequest.getMessage()).build();

//        return GreetingResponseDto.builder().message1(reqObj.getMessage1()).build();
//        return ResponseEntity.ok().body(GreetingResponseDto.builder().message1("ArmYo").build());


        return ResponseEntity.ok().body(resObj);
    }


    @PostMapping(value = "/test01")
    public ResponseEntity<Test01ResponseDto> test01(@Valid @RequestBody Test01RequestDto request) {
//        log.debug("0");

//        if(request.name == null || request.name.length() <= 0){
//            log.debug("1");
//            throw new BusinessException(ResponseModel.CODE_NAME_NOT_MATCH_TT0101, ResponseModel.MSG_NAME_NOT_MATCH_TT0101);
//        }

        if(!request.name.equals("Piyapat")){
            log.debug("2");
            throw new BusinessException(ResponseModel.CODE_NAME_NOT_MATCH_TT0101, ResponseModel.MSG_NAME_NOT_MATCH_TT0101, HttpStatus.BAD_GATEWAY);
        }

        Test01ResponseDto response = new Test01ResponseDto();
        response.setName(request.name);

        DataListItemModel dataListItemModel = new DataListItemModel();
        dataListItemModel.setNumber("1234");
        dataListItemModel.setStatus("IS OK");

        ArrayList<DataListItemModel> list = new ArrayList<>();
        list.add(dataListItemModel);
        response.setDataList(list);

//        resObj.setDataList(ArrayList<>dataListItemModel);


        return ResponseEntity.ok().body(response);

    }

    @PostMapping(value = "/vulcan.service")
    public ResponseEntity<VulcanResponseDto> VC01_VulcanMaster(@RequestBody VulcanRequestDto reqObj) {

        log.debug("reqObj.name : " + reqObj.name);

        VulcanResponseDto res = new VulcanResponseDto();

        Optional<VulcanMasterConfigProfile> vulcan = vulcanMasterConfigService.getVulcanByName(reqObj.name);
        res.name = vulcan.get().getName();
        res.value = vulcan.get().getValue();

        HttpHeaders headers = new HttpHeaders();
        headers.add("x-develop", "Piyapat");

//        return ResponseEntity.ok(res);
//        return ResponseEntity.status(HttpStatus.OK).body(res);
        return ResponseEntity.ok().headers(headers).body(res);
//        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(res);
    }

}
