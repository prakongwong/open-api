package com.promptnow.cb.openapi.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class BusinessException extends CommonException{

//    protected HttpStatus status = HttpStatus.OK;
    protected HttpStatus status;
    protected String code;

    public BusinessException(String code, String message, HttpStatus status){
        super(message);
        this.status = status;
        this.code = code;
    }

}
