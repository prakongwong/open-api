package com.promptnow.cb.openapi.dto;

import com.promptnow.cb.openapi.controller.model.ResponseModel;

import lombok.*;

import java.io.Serializable;

//@Data
//@Builder
//@ToString
public class GreetingResponseDto extends CommonResponseModel{
//public class GreetingResponseDto{

//    private Long id;
    public String message1;

//    public String code = ResponseModel.CODE_OPERATION_SUCCESS_0000;
//    public String message = ResponseModel.MSG_OPERATION_SUCCESS_0000;

    public GreetingResponseDto() {
        this.code = ResponseModel.CODE_OPERATION_SUCCESS_0000;
        this.message = ResponseModel.MSG_OPERATION_SUCCESS_0000;
    }

}
