package com.promptnow.cb.openapi.dto;

import com.promptnow.cb.openapi.controller.model.ResponseModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

//@Getter
//@Setter
@Data
public class Test01ResponseDto extends CommonResponseModel{

    private String name;
    private ArrayList<DataListItemModel> dataList;

    public Test01ResponseDto() {
        this.code = ResponseModel.CODE_OPERATION_SUCCESS_0000;
        this.message = ResponseModel.MSG_OPERATION_SUCCESS_0000;
    }


}
