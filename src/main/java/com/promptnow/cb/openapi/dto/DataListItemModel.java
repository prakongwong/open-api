package com.promptnow.cb.openapi.dto;

import lombok.Data;

@Data
public class DataListItemModel {

    private String number;
    private String status;
}
